using Assets.Scripts;
using Normal.Realtime;
using UnityEditor;
using UnityEngine;

namespace Assets.Editor
{
    [CustomEditor(typeof(DamageTracker), true)]

    public class DamageTrackerInspectorAddon : UnityEditor.Editor
    {
        private DamageTracker _damageTracker;
        Realtime realtime;
        void OnEnable()
        {
            _damageTracker = (DamageTracker) target;
        
            realtime = FindObjectOfType<Realtime>();
        
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            GUILayout.Space(20);
            GUILayout.Label("Debug tools", EditorStyles.boldLabel);
            //GUILayout.Space(10);
            if (_damageTracker.gameObject.activeSelf == false)
            {           
                GUILayout.Label("GameObject is disabled");
            }
            else if (realtime && realtime.connected)
            {
                GUILayout.Label("Health: " + _damageTracker.Health, EditorStyles.boldLabel);
                GUILayout.Label("Is dead: " + _damageTracker.IsDead, EditorStyles.boldLabel);
            }
            if (GUILayout.Button("Do damage"))
            {
                Debug.Log("Doing damage to object with button press");
                _damageTracker.AddDamage(1);
            }


        }
    }
}