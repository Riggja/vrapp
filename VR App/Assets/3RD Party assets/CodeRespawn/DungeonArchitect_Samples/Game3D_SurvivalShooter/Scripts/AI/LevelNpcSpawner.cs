//\$ Copyright 2015-22, Code Respawn Technologies Pvt Ltd - All Rights Reserved \$//\n
using UnityEngine;
using System.Collections.Generic;
using Normal.Realtime;

namespace DungeonArchitect.Samples.ShooterGame
{
	public class LevelNpcSpawner : DungeonEventListener {
		public GameObject parentObject;
		public GameObject[] npcTemplates;
		public Vector3 npcOffset = Vector3.zero;
	    public float spawnProbability = 0.25f;

		//public override void OnPostDungeonBuild (Dungeon dungeon, DungeonModel model)
		//{
		//	RebuildNPCs();
		//}

		public void RebuildNPCs() {
			DestroyOldNpcs();
			if (npcTemplates.Length == 0) return;

			var waypoints = GameObject.FindObjectsOfType<Waypoint>();
			Debug.Log(waypoints.Length);

			// Spawn an npc in each waypoint
			foreach (var waypoint in waypoints) {
	            if (Random.value < spawnProbability)
	            {
	                var position = waypoint.transform.position + npcOffset;
	                position = GetValidPointOnNavMesh(position);
	                var npcIndex = Random.Range(0, npcTemplates.Length);
	                var template = npcTemplates[npcIndex];
					var instantiateOptions = new Realtime.InstantiateOptions
					{
						destroyWhenLastClientLeaves = true, // If all players leave the game destroy the object
						destroyWhenOwnerLeaves = false,  // If the client that spawned this game object exits the object should not be destroyed
						ownedByClient = true // The client that spawns it should be responsible for updating it
					};
	                var npc = Realtime.Instantiate(template.name, position, Quaternion.identity, instantiateOptions) as GameObject;

	                if (parentObject != null)
	                {
	                    npc.transform.parent = parentObject.transform;
	                }
					var navAgent = npc.GetComponent<UnityEngine.AI.NavMeshAgent>();
					var warpResult = navAgent.Warp(position);
					if (!warpResult)
						Debug.LogError("Failed to warp!" + npc.name);
	            }
			}
		}

		Vector3 GetValidPointOnNavMesh(Vector3 position) {
			UnityEngine.AI.NavMeshHit hit;
			if (UnityEngine.AI.NavMesh.SamplePosition(position, out hit, 4.0f, UnityEngine.AI.NavMesh.AllAreas)) {
				return hit.position;
			}
			return position;
		}

		public override void OnDungeonDestroyed(Dungeon dungeon) {
			DestroyOldNpcs();
		}

		void DestroyOldNpcs() {
			if (parentObject == null) {
				return;
			}

			var npcs = new List<GameObject>();
			var parentTransform = parentObject.transform;
			for(int i = 0; i < parentTransform.childCount; i++) {
				var npc = parentObject.transform.GetChild(i).gameObject;
				npcs.Add(npc);
			}

			foreach (var npc in npcs) {
				if (Application.isPlaying) {
					Destroy(npc);
				} else {
					DestroyImmediate(npc);
				}
			}
		}
	}
}
