//\$ Copyright 2015-22, Code Respawn Technologies Pvt Ltd - All Rights Reserved \$//\n
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Normal.Realtime;

namespace DungeonArchitect.Samples.ShooterGame {
	public class GameController : MonoBehaviour {
		private static GameController instance;
		public Dungeon dungeon;
		private Realtime realtime;


		LevelNpcSpawner npcSpawner;
		string labelBuildingLayout = "Building Layout... ";
		string labelBuildingNavmesh = "Building Navmesh... ";


		public static GameController Instance {
			get {
				return instance;
			}
		}

		void Awake() {
			instance = this;
			realtime = FindObjectOfType<Realtime>();
			realtime.didConnectToRoom += CreateNewLevel;
			npcSpawner = GetComponent<LevelNpcSpawner>();
			
			
		}


		public void CreateNewLevel(Realtime r) {
			// Assing a different seed to create a new layout
			int seed = Mathf.FloorToInt(Random.value * int.MaxValue);
			dungeon.Config.Seed = (uint)seed;
			
			// Rebuild a new dungeon
			StartCoroutine(RebuildLevel(dungeon));
		}

        IEnumerator RebuildLevel(Dungeon dungeon) {
           

			yield return 0;
            
			dungeon.DestroyDungeon();
			yield return 0;

			dungeon.Build();
			

			yield return 0;
            
			npcSpawner.OnPostDungeonBuild(dungeon, dungeon.ActiveModel);

           
			// reset player health
			var player = GameObject.FindGameObjectWithTag(GameTags.Player);
			if (player != null) {
				var health = player.GetComponent<PlayerHealth>();
				if (health != null) {
					health.currentHealth = health.startingHealth;
				}
			}

			// Destroy any npc too close to the player
			var enemyControllers = GameObject.FindObjectsOfType<AIController>();
			var playerPosition = player.transform.position;
            foreach (var enemyController in enemyControllers)
            {
                var enemy = enemyController.gameObject;
				var distance = (playerPosition - enemy.transform.position).magnitude;
				if (distance < 1) {
					Destroy (enemy);
				}
			}
	    }
	}
}