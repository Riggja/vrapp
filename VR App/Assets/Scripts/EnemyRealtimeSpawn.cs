using UnityEngine;
using System;
using System.Collections.Generic;
using DungeonArchitect;
using Normal.Realtime;
using Assets.Scripts;

public class EnemyRealtimeSpawn : DungeonEventListener
{

    public GameObject parentObject;
    public NavigationBaker baker;

   public override void OnDungeonMarkersEmitted(Dungeon dungeon, DungeonModel model, LevelMarkerList markers)
    {   
       var instantiateOptions = new Realtime.InstantiateOptions
        {
            destroyWhenLastClientLeaves = true, // If all players leave the game destroy the object
            destroyWhenOwnerLeaves = false,  // If the client that spawned this game object exits the object should not be destroyed
            ownedByClient = true // The client that spawns it should be responsible for updating it
        };
        // Modify the markers list to add/remove/modify the markers in the scene before they are sent to the theme engine
       

        baker.BuildNavMesh();
        for(int i = 0; i<markers.Count; i++){
            if(markers[i].SocketType == "Enemy"){
                
                var npc = Realtime.Instantiate("Robot Warrior Enemy", markers[i].Transform.GetPosition(), Quaternion.identity, instantiateOptions) as GameObject;
                if (parentObject != null)
                {
                    npc.transform.parent = parentObject.transform;
                }

                var navAgent = npc.GetComponent<UnityEngine.AI.NavMeshAgent>();
				var warpResult = navAgent.Warp(markers[i].Transform.GetPosition());
            }
        }
        
    }
}