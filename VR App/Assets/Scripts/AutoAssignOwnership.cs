using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Normal.Realtime;
using UnityEngine;

public class AutoAssignOwnership : MonoBehaviour
{

    private Realtime realtime { get; set; }
    private RealtimeAvatarManager realtimeAvatarManager { get; set; }
    public RealtimeTransform realtimeTransform { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        realtime = FindObjectOfType<Realtime>();
        realtimeAvatarManager = FindObjectOfType<RealtimeAvatarManager>();
        realtimeTransform = GetComponent<RealtimeTransform>();
        realtime.didConnectToRoom += DidConnectToRoom;
        realtimeAvatarManager.avatarDestroyed += AvatarDestroyed;
    }

    private void AvatarDestroyed(RealtimeAvatarManager avatarManager, RealtimeAvatar avatar, bool islocalavatar)
    {
        if(avatarManager.avatars.Count == 0 || realtime.clientID == -1)
            return;

        //if (realtimeView.ownerIDInHierarchy == avatar.ownerIDInHierarchy)
        //{
            var firstAvatar = avatarManager.avatars.FirstOrDefault().Key;
            Debug.Log($"Avatar Destroyed. Calling Client: {realtime.clientID} Owner: {realtimeTransform.ownerIDInHierarchy}/{realtimeTransform.ownerIDInHierarchy}" );
            realtimeTransform.RequestOwnership(); 
            //}
    }

    private void DidConnectToRoom(Realtime r)
    {
        if (realtimeTransform.isUnownedSelf)
        {
            realtimeTransform.RequestOwnership();
        }

    }
}
