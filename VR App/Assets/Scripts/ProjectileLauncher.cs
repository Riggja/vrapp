using Normal.Realtime;
using System.Threading;

using UnityEngine;
using UnityEngine.Assertions;

namespace Assets.Scripts
{
    public class ProjectileLauncher : MonoBehaviour
    {

        [SerializeField] private Transform _muzzlePoint;
        [SerializeField] private RealtimeTransform _realtimeTransformOfOwner;
        [SerializeField] private RealtimeTransform _projectilePrefab;
        [SerializeField] private float _speed = 40.0f;
        // Start is called before the first frame update
        void Start()
        {
          Assert.IsNotNull(_projectilePrefab.GetComponent<Rigidbody>(), "projectile need to have a rigidbody");
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                FireProjectile();
            }
        }
        public void FireProjectile()
        {
            
            if (_realtimeTransformOfOwner.isOwnedLocallySelf)
            {
                // Instructions for how normcore should handle this item.
                // We only want normcore to automatically destroy the item 
                // when all players leave the game.
                var instantiateOptions = new Realtime.InstantiateOptions
                {
                    destroyWhenLastClientLeaves = true, // If all players leave the game destroy the object
                    destroyWhenOwnerLeaves = false,  // If the client that spawned this game object exits the object should not be destroyed
                    ownedByClient = true // The client that spawns it should be responsible for updating it
                };

                
                GameObject spawnedBullet = Realtime.Instantiate(_projectilePrefab.name,
                    _muzzlePoint.transform.position, _muzzlePoint.transform.rotation,
                instantiateOptions);

                
                
                spawnedBullet.GetComponent<Rigidbody>().velocity = _speed * _muzzlePoint.forward;

            }
        }
    }
}
