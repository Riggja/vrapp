using Autohand;
using Normal.Realtime;
using DungeonArchitect;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.Interaction.Toolkit;


namespace Assets.Scripts
{
    // Place on VR player, not the local in scene
    [RequireComponent(typeof(DamageTracker))]
    [DisallowMultipleComponent]
    public class Player : MonoBehaviour
    {
        
        private GameController game;

        private Realtime realtime;
        private SpawnPointManager _spawnPointManager;
        private FlashDamage _flashDamage;
        private SpawnPoint _closestSpawnpointWhenDied;
        private AutoHandPlayer _autoHandPlayer;
        private RealtimeTransform _realtimeTransform;
        private DamageTracker _damageTracker;
    
        // Start is called before the first frame update
        void Start()
        {
            realtime = FindObjectOfType<Realtime>();
            game = FindObjectOfType<GameController>();
            _realtimeTransform = GetComponent<RealtimeTransform>();
            if(_realtimeTransform.isOwnedLocallySelf)
                LocalStart();
        }

        public void LocalStart()
        {

            _autoHandPlayer = FindObjectOfType<AutoHandPlayer>();
            
            _flashDamage = Camera.main.GetComponentInChildren<FlashDamage>();
            if (!_flashDamage) Debug.LogError("The Required FlashDamage component was not found under the main camera.");

            _spawnPointManager = FindObjectOfType<SpawnPointManager>();
            if (!_spawnPointManager) Debug.LogError("Could not find a SpawnPointManager in the scene.");

            _damageTracker = GetComponent<DamageTracker>();
            
            var playerHealthGameObject = GameObject.FindGameObjectWithTag("PlayerHealth");
            if (!playerHealthGameObject) Debug.LogError("Could not find gameobject with tag playerHealthGameObject");

            var healthBar = playerHealthGameObject.GetComponentInChildren<Slider>();
            if (!healthBar) Debug.LogError("Could not find a Slider as a child of the playerHealthGameObject.");

            _damageTracker.healthBar = healthBar;
            _damageTracker.healthBar.maxValue = _damageTracker.MaxHealth;
            _damageTracker.healthBar.value = _damageTracker.Health;
                 
        }
        public void Update()
        {
            if (_realtimeTransform != null && _realtimeTransform.isOwnedLocallySelf && Input.GetKeyDown(KeyCode.Space))
            {
                Debug.Log("Dying");
                OnDying();
            }
        }
        public void OnTakeDamage()
        {
            Debug.Log("Player damage flash");
            if(_damageTracker.Health > 0)
                _flashDamage.Flash(0.35f);
        }
        public void OnDying()
        {
            Invoke("Respawn", 2f);
            _closestSpawnpointWhenDied = _spawnPointManager.GetClosest(transform.position);
            _autoHandPlayer.GetComponent<Rigidbody>().isKinematic = true;
            _autoHandPlayer.SetPosition(_closestSpawnpointWhenDied.transform.position);
            Debug.Log("On dying in player");
            if (_realtimeTransform.isOwnedLocallySelf == false) return;
            float flashTime = 3.0f;
            _flashDamage.Flash(flashTime);
           // _xrRig.transform.position = new Vector3(0, -203, 0);
            
        }

        private void Respawn()
        {
            _autoHandPlayer.GetComponent<Rigidbody>().isKinematic = false;
            _damageTracker.SetToAlive();
            _damageTracker.AddHealth(_damageTracker.MaxHealth);
            
            //_isDeadView.gameObject.SetActive(false);
        }

        void OnTriggerEnter(Collider collision)
        {
            //Check for a match with the specified name on any GameObject that collides with your GameObject
            if (collision.gameObject.tag == "Goal")
            {
                //If the GameObject's name matches the one you suggest, output this message in the console
                Debug.Log("You have reached the goal!");
                game.CreateNewLevel(realtime);
            }
        }
    }
}
