using System.Collections;
using System.Collections.Generic;

using Assets.Scripts;

using Normal.Realtime;

using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSource;
    private RealtimeTransform _realtimeTransform;
    [TagSelector]
    [SerializeField] private string[] _tagsOnObjectsThisCanDamage = new string[] { };
    // Start is called before the first frame update
    void Start()
    {
        _realtimeTransform = GetComponent<RealtimeTransform>();

        Invoke("SelfDestruct", 3);
    }

    void SelfDestruct()
    {

        Realtime.Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (_realtimeTransform.isOwnedLocallySelf)
        {
            var damageTracker = other.GetComponent<DamageTracker>();
            if (damageTracker)
                damageTracker.AddDamage(1);


            SelfDestruct();
        }


    }



}
