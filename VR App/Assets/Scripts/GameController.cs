using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DungeonArchitect;
using DungeonArchitect.Samples.ShooterGame;
using Normal.Realtime;

public class GameController : RealtimeComponent<SeedSyncModel> 
{
   private static GameController instance;
		public Dungeon dungeon;
		private Realtime realtime;
		private RealtimeTransform _realtimeTransform;

		


		public static GameController Instance {
			get {
				return instance;
			}
		}

		void Awake() {
			instance = this;
			realtime = FindObjectOfType<Realtime>();
			realtime.didConnectToRoom += CreateNewLevel;
			
			
		}


		public void CreateNewLevel(Realtime r) {
			_realtimeTransform = GetComponent<RealtimeTransform>();

			if (_realtimeTransform.isUnownedSelf)
            {
                _realtimeTransform.RequestOwnership();

            }

            if (_realtimeTransform.isOwnedLocallySelf)
            {
                // Assing a different seed to create a new layout
				model.seed = Mathf.FloorToInt(Random.value * int.MaxValue);
				
            }
            
			dungeon.Config.Seed = (uint)model.seed;
			// Rebuild a new dungeon
			StartCoroutine(RebuildLevel(dungeon));
		}

        IEnumerator RebuildLevel(Dungeon dungeon) {
           

			yield return 0;
            
			dungeon.DestroyDungeon();
			yield return 0;

			dungeon.Build();

            

	    }
}
