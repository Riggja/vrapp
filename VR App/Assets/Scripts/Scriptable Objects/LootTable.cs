using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;


[CreateAssetMenu(fileName = "LootTable", menuName = "There is no moon/LootTable")]

public class LootTable : ScriptableObject
{
    public List<LootItem> lootItems;

    public LootItem GetRandomItem()
    {
        float totalWeight = lootItems.Sum(item => item.weight);
        float randomNumber = Random.Range(0, totalWeight);
        float position = 0;
        foreach (LootItem item in lootItems)
        {
            // Shield Range 25
            // Health potion Range 25
            // Empty Range 50

            // 66
            position += item.weight;
            if (position >= randomNumber)
            {
                return item;
            } 
        }

        return null;
    }


}
[Serializable]
public class LootItem
{
    //A property public you capitalize first letter.
    public string description;
    public float weight;
    public GameObject item;
    public AudioClip audioClip;
}