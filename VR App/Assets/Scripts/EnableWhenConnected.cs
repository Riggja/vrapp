using System.Collections;
using System.Collections.Generic;
using Normal.Realtime;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

public class EnableWhenConnected : MonoBehaviour
{
    public UnityEvent onConnectedToNetwork;
    private Realtime realtime { get; set; }
    // Start is called before the first frame update
    void Start()
    {
        realtime = FindObjectOfType<Realtime>();
        realtime.didConnectToRoom += DidConnectToRoom;
        realtime.didDisconnectFromRoom += DidDisconnectFromRoom;
        
    }

    private void DidDisconnectFromRoom(Realtime r)
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_WEBPLAYER
         Application.OpenURL(webplayerQuitURL);
#else
         Application.Quit();
#endif
    }

    private void DidConnectToRoom(Realtime r)
    {
        Debug.Log("DidConnectToRoom");
        if (onConnectedToNetwork != null)
        {
            onConnectedToNetwork.Invoke();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
