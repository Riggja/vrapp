using System;
using System.Collections.Generic;
using UnityEngine;
using Normal.Realtime;
using UnityEngine.Assertions;

public class DisableComponentIfLocal : MonoBehaviour
{
    public List<GameObject> componentsToDisableIfLocal;
    private RealtimeView _realtimeView;
    //subscribe to events fired when an avatar is instantiated. 
    // If it was the local computers avatar that was instantiated, remove components that are already in scene.
    public void Start()
    {
        _realtimeView = GetComponent<RealtimeView>();
        Assert.IsNotNull(_realtimeView);
        if (_realtimeView.isOwnedLocallyInHierarchy)
        {
            foreach (var component in componentsToDisableIfLocal)
            {
                component.SetActive(false);
            }
        }
        else
        {
            Debug.Log("Not local avatar");
        }

    }

}
