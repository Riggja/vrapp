using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

using static UnityEngine.InputSystem.InputAction;

public class ToggleTeleportRay : MonoBehaviour
{
    private bool isEnabled;
    [SerializeField] private InputActionReference selectActionReference;
    XRRayInteractor rayInteractor;
    // Start is called before the first frame update
    void Awake()
    {
        rayInteractor = GetComponent<XRRayInteractor>();        
    }

    private void OnEnable()
    {
        selectActionReference.action.started += ToggleRay;
        selectActionReference.action.canceled += ToggleRay;
    }
    private void OnDisabled()
    {
        selectActionReference.action.started -= ToggleRay;
        selectActionReference.action.canceled -= ToggleRay;
    }

    
    private void LateUpdate()
    {
           rayInteractor.enabled = isEnabled;
    }

    private void ToggleRay(CallbackContext ctx)
    {
        isEnabled = ctx.control.IsPressed();
    }
}
