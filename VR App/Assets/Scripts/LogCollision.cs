using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogCollision : MonoBehaviour
{
    // Start is called before the first frame update
    void OnCollisionEnter(Collision collision)
    {
        if(collision.impulse.magnitude > 20)
            Debug.Log("Collision " + this.gameObject.name + " other " + collision.gameObject.name + " impulse " + collision.impulse + " magnitude" + collision.impulse.magnitude);
    }   

    // Update is called once per frame
    void Update()
    {
        
    }
}
