using UnityEngine;

namespace Assets.Scripts
{
    public class Weapon : MonoBehaviour
    {
        [SerializeField]  int _damage = 1;

        public int Damage { get { return _damage; }  }
    
    }
}
