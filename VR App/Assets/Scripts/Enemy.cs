﻿using System;
using System.Collections.Generic;

using NaughtyAttributes;

using Normal.Realtime;

using UnityEditor;

using UnityEngine;
using UnityEngine.AI;

using Random = UnityEngine.Random;

// ReSharper disable ConvertToConstant.Local

namespace Assets.Scripts
{
    [RequireComponent(typeof(NavMeshAgent))]
    [RequireComponent(typeof(RealtimeView))]
    [RequireComponent(typeof(RealtimeTransform))]
    [RequireComponent(typeof(Animator))]
    //[RequireComponent(typeof(AutoAssignOwnership))]
    [RequireComponent(typeof(DamageTracker))]
    public class Enemy : RealtimeComponent<EnemySyncModel>
    {
        [SerializeField] private float _maxDistanceToNextRandomWalkTarget = 10f;

        [Header("Attack range, see view distance for detection range.")]
        [SerializeField] private float _attackDistance = 5.0f;
        [SerializeField] private float _attackCoolDown = 1.0f;
        [SerializeField] private float _minTimeToWaitAtWaypoint = 1.0f;
        [SerializeField] private float _maxTimeToWaitAtWaypoint = 3.0f;
        [SerializeField] private Transform _testTransform;
        public float _visionAngle = 0;

        [SerializeField] private float _visionRange = default;
        [SerializeField] private WaypointNetwork _EnemyWaypoints;
        private Transform playerTransform;

        public BehaviorType _behaviorType = BehaviorType.RANDOM_WALKER;

        // internal

        private Vector3 _randomTarget;
        private float _distanceToPlayer;
        private RealtimeAvatarManager _realtimeAvatarManager;
        private Dictionary<int, RealtimeAvatar> _avatars;
        private int _currentWaypoint = -1;
        private float _timeSinceLastAttack = 0f;
        private DamageTracker _damageTracker;
        private Animator _animator;
        private NavMeshAgent _navAi = default;
        
        private RealtimeTransform _realtimeTransform;

        public enum BehaviorType
        {
            GUARD = 0,
            RANDOM_WALKER = 1
        }


        [Header("Debug")][SerializeField] private bool showAggroRange = false;
        [ReadOnly][SerializeField] private float _timeLeftToWaitAtWypoint = 0.0f;
        [SerializeField][ReadOnly] private Transform _chasedAvatar = default;

        public Enemy(WaypointNetwork enemyWaypoints)
        {
            this._EnemyWaypoints = enemyWaypoints;
        }

        // Start is called before the first frame update

        private void Awake()
        {
            _animator = GetComponent<Animator>();
            // Get the Realtime component on this game object
            _realtimeAvatarManager = FindObjectOfType<RealtimeAvatarManager>();
            _realtimeTransform = GetComponent<RealtimeTransform>();
            _navAi = GetComponent<NavMeshAgent>();
            _damageTracker = GetComponent<DamageTracker>();
            _timeLeftToWaitAtWypoint = Random.Range(_minTimeToWaitAtWaypoint, _maxTimeToWaitAtWaypoint);
            playerTransform = Camera.main.transform;
        }

        void Update()
        {
            //if (_chasedAvatar)
            //    //_navAi.stoppingDistance = _attackDistance;
            //else
            //{
            //    //_navAi.stoppingDistance = 1.0f;
            //}

            // Only the client responsible for updating this enemy should run the update function.
            if (_realtimeTransform.isOwnedLocallySelf == true)
                LocalUpdate();
            
        }

        private void LocalUpdate()
        {
            if (_damageTracker.IsDead)
            {
                _navAi.enabled = false;
                return;
            }

            // Update the networked variable speed with the current speed of the navmesh agent.
            model.speed = _navAi.velocity.magnitude;
            _timeSinceLastAttack += Time.deltaTime;


            // If this enemy is currently not chasing a player, see if we can find one within range.
            if (!_chasedAvatar)
            {
                FindPossiblePlayerToChase();
            }

            // If we have a player to chase, hunt it.
            if (_chasedAvatar)
            {
                HuntAndKillPlayer();
                return;
            }

            if (_EnemyWaypoints)
                HandleWaypointNavigation();
            else HandleRandomWalkerAi();
        }

        private bool PointWithinViewCone(Vector3 point)
        {
            Vector3 direction = (point - this.transform.position).normalized;
            // Dot product determin how aligned two vectors are,  1 if parallel 0 if perpendicula -1 opposite direction
            float cosAngle = Vector3.Dot(direction, this.transform.forward);

            // get the angle in radians whose cosine is cosAngle
            float angle = Mathf.Acos(cosAngle) * Mathf.Rad2Deg;

            return angle < _visionAngle;
        }

        private void HandleRandomWalkerAi()
        {
            if (_behaviorType != BehaviorType.RANDOM_WALKER)
                return;

            //Debug.Log(
            //    $"{gameObject.name} Remaining distance" + _navAi.remainingDistance + "status; " + _navAi.path.status);
            if (_navAi.isOnNavMesh && _navAi.remainingDistance <= _navAi.stoppingDistance)
            {
                _timeLeftToWaitAtWypoint -= Time.deltaTime;

                if (_timeLeftToWaitAtWypoint <= 0)
                {
                    var randomPointInCircleAroundEntity = Random.insideUnitCircle * _maxDistanceToNextRandomWalkTarget;
                    var newDestinationSet = _navAi.SetDestination(transform.position +
                                                        new Vector3(randomPointInCircleAroundEntity.x, 0,
                                                            randomPointInCircleAroundEntity.y));

                    _randomTarget = transform.position +
                                    new Vector3(randomPointInCircleAroundEntity.x, 0, randomPointInCircleAroundEntity.y);

                    // Only reset wait time if we found a target, otherwise try to find a place to walk to next frame
                    if (newDestinationSet)
                        _timeLeftToWaitAtWypoint = Random.Range(_minTimeToWaitAtWaypoint, _maxTimeToWaitAtWaypoint);

                    //Debug.Log("Time to wait at next waypoint" + _timeLeftToWaitAtWypoint);


                }
            }

        }
        // If we now have a player to chase, check if it is close enough to engage, otherwise remove chased avatar.
        private void HuntAndKillPlayer()
        {
            Vector2 playerPosition2D =
                new Vector2(_chasedAvatar.transform.position.x, _chasedAvatar.transform.position.z);
            Vector2 enemyPosition2D = new Vector2(transform.position.x, transform.position.z);
            _distanceToPlayer = Vector2.Distance(playerPosition2D, enemyPosition2D);
            transform.LookAt(new Vector3(_chasedAvatar.position.x, transform.position.y,
                _chasedAvatar.position.z));
            //Debug.Log("Looking at player at " + _chasedAvatar.transform.position);
            if (CanAttack(_distanceToPlayer))
            {
                // Rotate towards the player when fighting.
                //Debug.Log(
                //    $"Distance to player {(_distanceToPlayer <= _attackDistance + 0.5)}_timeSinceLastAttack {_timeSinceLastAttack} >= _attackCoolDown {(_timeSinceLastAttack >= _attackCoolDown)}");
                _timeSinceLastAttack = 0;
                //if (_navAi.enabled)
                    //_navAi.isStopped = true;
                model.attackTrigger = !model.attackTrigger;
                //Debug.Log("Distance to player when attacking" + _distanceToPlayer);
            }
            
            // If the player is nearby, chase the player
            else if (PlayerWithinChasingDistance(_distanceToPlayer) && _distanceToPlayer < _visionRange && _navAi.enabled && _navAi.isOnNavMesh)
            {
                var result = _navAi.SetDestination(_chasedAvatar.transform.position);
                _navAi.isStopped = false;
                //Debug.Log($"Setting navmesh target to {_chasedAvatar.transform.position} status: {result}" );
                
            }

            if (PlayerWithinChasingDistance(_distanceToPlayer) == false)
            {

                _chasedAvatar = null;
            }
        }

        private bool PlayerWithinChasingDistance(float distanceToPlayer)
        {
            return distanceToPlayer <= _visionRange;
        }

        private bool CanAttack(float distanceToPlayer)
        {
            double attackTime = (_damageTracker.NextTimeThisCanTakeDamage + .1);


            if (attackTime > Time.fixedUnscaledTime)
            {
              //  Debug.Log("Not time to attack");
                return false;
            }

            //Debug.Log("Is within attack distance: " + (distanceToPlayer <= _attackDistance));
            return distanceToPlayer <= _attackDistance + 0.5 && _timeSinceLastAttack >= _attackCoolDown;
        }

        private void HandleWaypointNavigation()
        {
            if (_navAi.hasPath && !(_navAi.remainingDistance <= _navAi.stoppingDistance)) return;
            //Debug.Log($"Changing waypoint for: {gameObject.name} has path: {_navAi.hasPath }");
            _currentWaypoint++;
            if (_currentWaypoint == _EnemyWaypoints.WaypointPositions.Count)
            {
                _currentWaypoint = 0;
            }


            var destSet = _navAi.SetDestination(_EnemyWaypoints.WaypointPositions[_currentWaypoint].transform.position);
            //Debug.Log($"New waypoint targe: {gameObject.name} going to {_currentWaypoint} dest set {destSet}");
        }

        private void FindPossiblePlayerToChase()
        {
            float closestPlayerDistance = Single.MaxValue;

            foreach (var avatar in _realtimeAvatarManager.avatars.Values)
            {
                float distance = Vector3.Distance(avatar.head.position, transform.position);

                // Player is close enough to chase and closer than previously closest player
                if (distance < _visionRange && distance < closestPlayerDistance)
                {
                    if (PointWithinViewCone(avatar.head.position))
                    {
                        _chasedAvatar = avatar.head;
                        closestPlayerDistance = distance;
                        Debug.Log("Found player to chase!");
                    }


                }
            }
        }

        public void OnDrawGizmos()
        {
#if UNITY_EDITOR
            if (showAggroRange)
            {
                DrawAggroRange();
                DrawViewCone();
            }

            if (_navAi)
            {
                Gizmos.color = Color.white;
                Handles.DrawPolyLine(_navAi.path.corners);


                if (!_chasedAvatar)
                {
                    Handles.color = Color.yellow;
                    Handles.DrawSolidDisc(_randomTarget, Vector3.up, 0.3f);
                }
                else if (_chasedAvatar)
                {
                    Handles.color = Color.red;
                    Handles.DrawSolidDisc(_chasedAvatar.position, Vector3.up, 0.3f);
                }
            }
#endif
        }

        private void DrawViewCone()
        {
            
            bool targetSeen = false;
            Transform testTransform = _chasedAvatar ? _chasedAvatar : _testTransform;
            
            if (testTransform)
            {
                targetSeen = PointWithinViewCone(testTransform.position);
                //if (!targetSeen)
                //{
                //    Debug.Log("Not within view cone");
                //}
                //if(!(Vector3.Distance(transform.position, testTransform.position) < _visionRange))
                //    Debug.Log("Not within view dist");

                targetSeen = targetSeen && Vector3.Distance(transform.position, testTransform.position) < _visionRange;
            }
            
            //  parametric sphere equation 
            var xAngle = _visionAngle + transform.rotation.eulerAngles.y;
            float x = transform.position.x + _visionRange * Mathf.Sin(xAngle * Mathf.Deg2Rad) * Mathf.Cos(0 * Mathf.Deg2Rad);
            float y = transform.position.y + _visionRange * Mathf.Sin(xAngle * Mathf.Deg2Rad) * Mathf.Sin(0 * Mathf.Deg2Rad);
            float z = transform.position.z + _visionRange * Mathf.Cos(xAngle * Mathf.Deg2Rad);

            Debug.DrawLine(transform.position, new Vector3(x, y, z), targetSeen ? Color.red : Color.green);

            xAngle = -_visionAngle + transform.rotation.eulerAngles.y;
            float x2 = transform.position.x + _visionRange * Mathf.Sin(xAngle * Mathf.Deg2Rad) * Mathf.Cos(0 * Mathf.Deg2Rad);
            float y2 = transform.position.y + _visionRange * Mathf.Sin(xAngle * Mathf.Deg2Rad) * Mathf.Sin(0 * Mathf.Deg2Rad);
            float z2 = transform.position.z + _visionRange * Mathf.Cos(xAngle * Mathf.Deg2Rad);

            Debug.DrawLine(transform.position, new Vector3(x2, y2, z2), targetSeen ? Color.red : Color.green);

        }
        private void DrawAggroRange()
        {
            Gizmos.color = new Color(1f, 0f, 0f, 0.07f);
            Gizmos.DrawSphere(transform.position, _attackDistance);
            Gizmos.color = new Color(0f, 1f, 0f, 0.03f);
            Gizmos.DrawSphere(transform.position, _visionRange);
        }

        // This is called when the game starts on this client. Also called when a networked
        // model is replaced, but not using that functionality.
        protected override void OnRealtimeModelReplaced(EnemySyncModel previousModel,
            EnemySyncModel currentModel)
        {

            if (previousModel != null)
            {
                // Unregister from events
                previousModel.speedDidChange -= SpeedDidChange;
                previousModel.attackTriggerDidChange -= AttackTriggerDidChange;
            }

            if (currentModel != null)
            {
                // A fresh model is one that has not been instantiated by any client. If the model is not fresh it means
                // that this GameObject has been instantiated by this or another client.
                if (currentModel.isFreshModel)
                {
                    //Debug.Log($"Fresh damage tracker, health is now {currentModel.health} and should be the same as {m_MaxHealth} on game object {gameObject.name}");
                }
                else
                {
                    //      Debug.Log($"OnRealtimeModelReplaced existing model, health is now {currentModel.health} and should be the same as {m_MaxHealth} on game object {gameObject.name}");
                    _navAi.Warp(transform.position);
                }

                // Subscribe to receive events when speed or attack trigger variables are updated
                currentModel.speedDidChange += SpeedDidChange;
                currentModel.attackTriggerDidChange += AttackTriggerDidChange;

            }
        }


        // Network callback. Whenever the value of model.attackTrigger is changed on a client, all clients get this
        // event sent out and can play the attack animation locally.
        private void AttackTriggerDidChange(EnemySyncModel enemySyncModel, bool value)
        {
            if (_animator)
            {
                //Debug.Log("Attack Trigger");
                _animator.SetTrigger("Attack");
            }
        }

        // Whenever the network synced variable model.speed is changed, all clients get this event sent
        // and can update the speed at which the animation is playing on their computer.
        private void SpeedDidChange(EnemySyncModel enemySyncModel, float value)
        {
            if (_animator)
                _animator.SetFloat("Speed", value);
        }
    }
}