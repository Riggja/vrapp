using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    private AudioSource _audioSource;
    public GameObject SpawnPointVisualObject;
    public bool active = true;
    public void ActivatePortal()
    {
        
        Debug.Log("ActivatePortal called " + gameObject.name);
        if (active)
            return;

        active = true;
        _audioSource = GetComponent<AudioSource>();
        if(_audioSource)
            _audioSource.Play();
        Debug.Log("Portal activated " + gameObject.name);
        
    }


    SpawnPointManager _spawnPointManager;
    // Start is called before the first frame update
    void Start()
    {
        _spawnPointManager = FindObjectOfType<SpawnPointManager>();
        _spawnPointManager.RegisterSpawnPoint(this);

        if(SpawnPointVisualObject)
            SpawnPointVisualObject.SetActive(active);
    }
}
