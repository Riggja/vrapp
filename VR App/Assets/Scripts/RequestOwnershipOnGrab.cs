using Autohand;
using Normal.Realtime;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class RequestOwnershipOnGrab : MonoBehaviour
{

    /// <summary>
    /// For a client to be able to update the position of an object in the world,
    /// that client need to have ownership of it. This function is used to request
	/// ownership of an object when it is selected by by an XRBaseInteractor component 
	/// like XRGrabInteractor or XRRayInteractor.  
	/// The script can be placed on some independent game object in the scene, for 
	/// example the XR Interaction Manager.Then on the interactor components SelectEnter
	/// field add a call to this method.
    /// </summary>
    /// <param name="args">SelectEnterEventArgs sent by an XR Interactor component when 
	/// it selects something. </param>
    public void RequestOwnership(Hand hand, Grabbable grabbable)
    {
        
        var realtimeTransform = grabbable.transform.GetComponent<RealtimeTransform>();
        
        
        if (realtimeTransform)
        {
            Debug.Log($"RequestOwnership on on {grabbable.transform.name}");
            realtimeTransform.RequestOwnership();
        }
        else
        {
            Debug.Log("Failed to find realtime transform");
        }
    }
}
