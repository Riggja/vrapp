using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

public class WaypointNetwork : MonoBehaviour
{

    public List<Transform> WaypointPositions;


    public void OnDrawGizmos()
    {
#if UNITY_EDITOR

        for (int i = 0; i < WaypointPositions.Count; i++)
        {
            Handles.color = Color.yellow;
            Vector3 start = WaypointPositions[i].position;
            Vector3 end;
            if (i + 1 != WaypointPositions.Count)
            {
                end = WaypointPositions[i + 1].position;
            }
            else
            {
                end = WaypointPositions[0].position;
            }

            // Fetch the position of the waypoint for this iteration and
            // copy into our vector array.

            NavMeshPath path = new NavMeshPath();
            var result = NavMesh.CalculatePath(start, end,
                UnityEngine.AI.NavMesh.AllAreas, path);

            Handles.DrawPolyLine(path.corners);
        }
#endif
    }
}
