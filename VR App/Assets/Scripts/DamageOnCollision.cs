using Normal.Realtime;
using UnityEngine;

namespace Assets.Scripts
{
    public class DamageOnCollision : MonoBehaviour
    {
        [TagSelector]
        [SerializeField] private string[] _tagsToAddDamageToOnCollision = new string[] { };
        private RealtimeTransform _realtimeTransform;

        // Start is called before the first frame update
        void Start()
        {
            _realtimeTransform = GetComponent<RealtimeTransform>();
        }

        private void OnCollisionEnter(Collision other)
        {
            Debug.Log("DamageOnCCollision with " +other.gameObject.name);

            var damageTracker = other.gameObject.GetComponentInChildren<DamageTracker>();
            if (damageTracker)
            {
                Debug.Log($"damage tracker found on {other.gameObject.name} adding damage");
                damageTracker.AddDamage(2);
            }
            else
            {
                Debug.Log("No DamageTracker found in children" + damageTracker);
            }
        }
    }
}
