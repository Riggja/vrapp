using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using Unity.AI.Navigation;
using UnityEngine;
using DungeonArchitect;
using DungeonArchitect.Samples.ShooterGame;
using Autohand;
using Normal.Realtime;

namespace Assets.Scripts
{
    public class NavigationBaker : DungeonEventListener
    {

        public NavMeshSurface[] surfaces;
        public GameObject parentObject;

        private RealtimeTransform _realtimeTransform;
        private SpawnPoint _GetSpawn;
        private SpawnPointManager _spawnPointManager;
        private AutoHandPlayer _autoHandPlayer;
        private List<GameObject> spawnedEnemies = new List<GameObject>();

        public override void OnPostDungeonBuild(Dungeon dungeon, DungeonModel model)
        {
            _realtimeTransform = GetComponent<RealtimeTransform>();

            Debug.Log("Building nav mesh, owner is client id: " + _realtimeTransform.ownerIDSelf);
            BuildNavMesh();

            if (_realtimeTransform.isUnownedSelf)
            {
                _realtimeTransform.RequestOwnership();

            }

            if (_realtimeTransform.isOwnedLocallySelf)
            {
                InstantiateEnemies(dungeon);
            }

            // Spawn Player Here
            _autoHandPlayer = FindObjectOfType<AutoHandPlayer>();
            _spawnPointManager = FindObjectOfType<SpawnPointManager>();
            _GetSpawn = _spawnPointManager.GetClosest(transform.position);
            _autoHandPlayer.SetPosition(_GetSpawn.transform.position);
        }

        private void InstantiateEnemies(Dungeon dungeon)
        {
            var instantiateOptions = new Realtime.InstantiateOptions
            {
                destroyWhenLastClientLeaves = true, // If all players leave the game destroy the object
                destroyWhenOwnerLeaves = false, // If the client that spawned this game object exits the object should not be destroyed
                ownedByClient = true // The client that spawns it should be responsible for updating it
            };

            for (int i = 0; i < dungeon.Markers.Count; i++)
            {
                if (dungeon.Markers[i].SocketType == "Enemy")
                {
                    var npc = Realtime.Instantiate("Robot Warrior Enemy", dungeon.Markers[i].Transform.GetPosition(), Quaternion.identity, instantiateOptions) as GameObject;
                    spawnedEnemies.Add(npc.gameObject);
                    if (parentObject != null)
                    {
                        npc.transform.parent = parentObject.transform;
                    }

                    var navAgent = npc.GetComponent<UnityEngine.AI.NavMeshAgent>();
                    var warpResult = navAgent.Warp(dungeon.Markers[i].Transform.GetPosition());
                }
            }
        }

        public override void OnDungeonDestroyed(Dungeon dungeon)
        {
            if (_realtimeTransform && _realtimeTransform.isOwnedLocallySelf)
            {
                for (int i = 0; i < spawnedEnemies.Count; i++)
                {
                    Realtime.Destroy(spawnedEnemies[i].gameObject);
                }
            }
        }

        // Use this for initialization
        public void BuildNavMesh()
        {
            if (surfaces == null || surfaces.Length == 0)
                surfaces = GetComponentsInChildren<NavMeshSurface>();
            for (int i = 0; i < surfaces.Length; i++)
            {
                surfaces[i].BuildNavMesh();
            }
        }

    }
}