using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalManager : MonoBehaviour
{
   
    void OnTriggerEnter(Collider collision)
    {
        //Check for a match with the specified name on any GameObject that collides with your GameObject
        if (collision.gameObject.tag == "Goal")
        {
            //If the GameObject's name matches the one you suggest, output this message in the console
            Debug.Log("You have reached the goal!");
        }
    }

}
