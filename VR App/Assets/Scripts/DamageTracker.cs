using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Normal.Realtime;

using TMPro;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using UnityEngine.UI;
// ReSharper disable FieldCanBeMadeReadOnly.Local
// ReSharper disable ConvertToConstant.Local

namespace Assets.Scripts
{
    public class DamageTracker : RealtimeComponent<DamageTrackerSyncModel>
    {
        enum DamageDetectionMode
        {
            CollisionEnter,
            TriggerEnter

        }
        public LootTable lootTable;

        [SerializeField] public int MaxHealth = default;
        [SerializeField] private float _timeToWaitBeforeSpawningLoot = 0.5f;
        [SerializeField] private Color _colorToFlashWhenTakingDamage = Color.red;
        [SerializeField] private DamageDetectionMode _damageDetectionMode = DamageDetectionMode.CollisionEnter;
        [SerializeField] private float _timeToWaitBeforeItCanTakeDamageAgain = 0.5f;
        [SerializeField] private AudioClip _audioClipForHit = null;
        [SerializeField] private TextMeshPro _healthDisplayText = null;
        [SerializeField] private bool _displayHealthBarOnStart = false;
        [Tooltip("call this function on all entities in the network instead of the default one. " +
                 "To call the default and perform additional things add this class's OnDying to " +
                 "the UnityEvent and then add the other methods you want to call.")]
        [Header("Use to replace default OnDying method.")]
        public UnityEvent onDyingOverride;

        [Header("Additional functions owner client calls when damaged.")]
        public UnityEvent onTakeDamage;
        private bool _firstHit = true;
        [TagSelector]
        [SerializeField] private string[] _tagsDamageOnCollision = new string[] { };
        protected List<Renderer> rendererList = new List<Renderer>();


        // Internal
        public float NextTimeThisCanTakeDamage { get; private set; }
        protected RealtimeTransform RealtimeTransform;
        private AudioSource _effectsAudioSource;
        private List<Color> _originalColors = new List<Color>();
        private Animator _animator;
        public Slider healthBar;
        private static readonly int Emission = Shader.PropertyToID("_EMISSION");
        private static readonly int EmissionColor = Shader.PropertyToID("_EmissionColor");

        public bool IsDead => model.isDead;

        public int Health => model.health; //make this readable so that other scripts can access the Health values

        public void Awake()
        {
            RealtimeTransform = GetComponent<RealtimeTransform>();
            _animator = GetComponent<Animator>();
            if(!healthBar)
                healthBar = GetComponentInChildren<Slider>();
            GetRenderersIfListIsEmpty();
            if (healthBar)
            {
                healthBar.gameObject.SetActive(_displayHealthBarOnStart);
            }
        }
        protected void GetRenderersIfListIsEmpty()
        {
            if (rendererList.Count != 0)
                return;

            //Debug.LogError("Emission keyword value " + Emission + " color" + EmissionColor);
            foreach (Renderer objectRenderer in gameObject.GetComponentsInChildren<Renderer>())
            {
                objectRenderer.material.EnableKeyword("_EMISSION");
                if (objectRenderer.material.HasProperty("_EmissionMap"))
                {
                    rendererList.Add(objectRenderer);
                }
            }
            foreach (var r in rendererList)
            {
                _originalColors.Add(r.material.GetColor(EmissionColor));
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (_damageDetectionMode != DamageDetectionMode.TriggerEnter)
                return;

            if (_tagsDamageOnCollision.Any(x => other.gameObject.CompareTag(x))) OnHit(other.gameObject);

        }

        private void OnCollisionEnter(Collision other)
        {
            if (_damageDetectionMode != DamageDetectionMode.CollisionEnter)
                return;
            

            if (_tagsDamageOnCollision.Any(x => other.gameObject.CompareTag(x))) OnHit(other.gameObject);
        }

        private void OnHit(GameObject other)
        {
            Debug.Log("Match tag " + other.gameObject.name + " tag " + other.gameObject.tag);
            if (RealtimeTransform && RealtimeTransform.isOwnedLocallySelf == false) return;
            Weapon weapon = other.gameObject.GetComponent<Weapon>();
            if (weapon)
            {
                Debug.Log("Has weapon " + other.gameObject.name + " tag " + other.gameObject.tag);
                onTakeDamage?.Invoke();
                AddDamage(weapon.Damage);
            }
            else
            {
                Debug.LogWarning("No weapon script on other object, no damage. If this was meant to do damage add the Weapon script to it." + other.gameObject.name + " tag " + other.gameObject.tag);
            }
        }

        public virtual void SetToAlive()
        {
            model.isDead = false;
        }
        public virtual bool AddHealth(int healing)
        {
            if (model.isDead)
            {
                Debug.Log("Trying to heal dead " + gameObject.name);
                return false;
            }

            if (model.health <= MaxHealth)
            {
                model.health = Mathf.Min(MaxHealth, model.health + healing);
                return true;
            }
            return false;
        }


        public virtual bool AddDamage(int damage)
        {
            // Don't want every client to add to the health when one person hit the enemy. 
            if (RealtimeTransform && RealtimeTransform.isOwnedLocallySelf == false) return false;

            if (model.isDead)
                return false;

            if (NextTimeThisCanTakeDamage > Time.fixedUnscaledTime)
            {
                return false;
            }

            NextTimeThisCanTakeDamage = Time.fixedUnscaledTime + _timeToWaitBeforeItCanTakeDamageAgain;
            // If false set to true, otherwise set to false. When hitTrigger changes value a DidChange event is sent
            // to all clients
            model.hitTrigger = !model.hitTrigger;
            model.health -= damage;
            if (model.health <= 0)
            {
                model.isDead = true;
            }
            Debug.Log("On trigger enter called on damage tracker and damage decreased. New damage: " + model.health);
            return true;
        }

        public void DisableColliders()
        {
            var colliders = GetComponentsInChildren<Collider>();
            foreach (var coll in colliders)
            {
                coll.enabled = false;
            }
        }
        public virtual void OnDying()
        {
            _animator.SetTrigger("Died");

            DisableColliders();

            // If there is a loot table associated with this one.
            if (lootTable)
            {
                Invoke("SpawnLoot", _timeToWaitBeforeSpawningLoot);
            }

            if (healthBar)
            {
                Debug.Log("Disabling health bar");
                healthBar.gameObject.SetActive(false);
            }

            Invoke("OnDead", 10);
        }

        private void SpawnLoot()
        {
            // Find a random item or null from the loot table
            LootItem lootInfo = lootTable.GetRandomItem();

            GameObject spawnLootPrefab = lootInfo.item;

            // Null would mean that this roll did not result in a loot drop.
            if (spawnLootPrefab != null)
            {
                // Realtime.Instantiate will spawn the object and make it visible for all players.
                // We only want the client responsible for this object to take spawn the object, otherwise
                // we get multiple things instead of one.
                Debug.Log("PrefabSpawn");

                if (RealtimeTransform && RealtimeTransform.isOwnedLocallySelf == true)
                {
                    // Instructions for how normcore should handle this item.
                    // We only want normcore to automatically destroy the item 
                    // when all players leave the game.
                    var instantiateOptions = new Realtime.InstantiateOptions
                    {
                        destroyWhenLastClientLeaves = true,
                        destroyWhenOwnerLeaves = false,
                        ownedByClient = false
                    };


                    var colliderBounds = transform.GetComponent<Collider>().bounds.max;
                    GameObject spawnedItem = Realtime.Instantiate(spawnLootPrefab.name,
                        new Vector3(transform.position.x, colliderBounds.y, transform.position.z), Quaternion.identity,
                        instantiateOptions);

                    Rigidbody rigidbodyItem = spawnedItem.GetComponent<Rigidbody>();
                    if (rigidbodyItem)
                    {
                        rigidbodyItem.AddForce(0, 7, 0, ForceMode.Impulse);
                    }
                }

                if (lootInfo.audioClip)
                {
                    this.PlayAudio(lootInfo.audioClip);
                }
            }
        }

        protected virtual void OnDead()
        {
            var renders = GetComponentsInChildren<Renderer>();
            foreach (var render in renders)
            {
                render.enabled = false;
            }
        }
        /// <summary>
        /// When a RealtimeComponent is first created, it starts with no model. Normcore populates
        /// it once we have successfully connected to the server (or instantly if we're already
        /// connected), and calls OnRealtimeModelReplaced() to provide us with a copy of it.
        /// If this RealtimeComponent was previously mapped to a different model (e.g. when
        /// switching rooms), it will provide a reference to the previous model in order to
        /// allow your component to unregister it from events.
        /// </summary>
        /// <param name="previousModel">Model that existed in another room</param>
        /// <param name="currentModel">The model that exist in this room</param>
        protected override void OnRealtimeModelReplaced(DamageTrackerSyncModel previousModel,
            DamageTrackerSyncModel currentModel)
        {
            // A previous model exist when for example this entity moves from one room to another.
            if (previousModel != null)
            {
                // Unregister from events
                previousModel.healthDidChange -= HealthDidChange;
                previousModel.hitTriggerDidChange -= HitTriggerDidChange;
            }

            if (currentModel != null)
            {
                // A fresh model is one that has not been instantiated by any client. If the model is not fresh it means
                // that this GameObject has been instantiated by this or another client.
                if (currentModel.isFreshModel)
                {
                    currentModel.health = MaxHealth;
                    if (healthBar)
                    {
                        healthBar.maxValue = MaxHealth;
                    }
                }

                // If this is an object in the scene, deactivate it once instantiated if health < 0 
                if (currentModel.health <= 0 && realtimeView.isRootSceneView)
                {

                    OnDead();
                    return;
                }

                // Update the materials to match the most health status
                UpdateHealthDisplay(model.health);


                // Register for events if we want to take action on health change.
                currentModel.healthDidChange += HealthDidChange;
                currentModel.hitTriggerDidChange += HitTriggerDidChange;
            }
        }

        private void HitTriggerDidChange(DamageTrackerSyncModel damageTrackerSyncModel, bool value)
        {
            if (_animator)
                _animator.SetTrigger("Take Damage");

            StartCoroutine(FlashObject(_colorToFlashWhenTakingDamage, _timeToWaitBeforeItCanTakeDamageAgain, .07f));
            if (_audioClipForHit)
                PlayAudio(_audioClipForHit);
        }


        void UpdateHealthDisplay(int health)
        {
            if (healthBar)
            {
                if (health < MaxHealth && _firstHit)
                {
                    _firstHit = false;
                    healthBar.gameObject.SetActive(true);
                }
                healthBar.value = health;
            }

            if (_healthDisplayText)
            {
                _healthDisplayText.text = health.ToString();
            }
        }
        private void HealthDidChange(DamageTrackerSyncModel model, int health)
        {
            Debug.Log($"HealthDidChange callback. New hp {health} new nw health {model.health} {gameObject.name}");
            UpdateHealthDisplay(health);
            
            if (model.health <= 0)
            {
                
                if (onDyingOverride != null && onDyingOverride.GetPersistentEventCount() > 0 )
                {                    
                    onDyingOverride.Invoke();
                }
                else
                {
                    OnDying();
                }                
            }
        }

        private void PlayAudio(AudioClip audioClip)
        {
            // If there is no audioclip, just return and do not try to create it 
            if (audioClip == null)
                return;

            // Check if we already have an AudioSource on this object, if not set up a new one
            if (_effectsAudioSource == null)
                CreateEffectsAudioSource();

            // We now know that we have an audio clip and an AudioSource, so we can play the sound
            _effectsAudioSource.PlayOneShot(audioClip);
        }

        void CreateEffectsAudioSource()
        {
            // We call this function if no AudioSource has been created earlier for our code
            // The first thing that happens is that we add an AudioSource to our GameObject
            // and then assign a reference to the variable _effectsAudioSource so we can
            // do stuff with it.
            _effectsAudioSource = gameObject.AddComponent<AudioSource>();
            // We do not want our AudioSource to play sounds repeatedly, just once
            _effectsAudioSource.loop = false;
            // We do not want this to automatically play a sounds when it is created,
            // we will control that. 
            _effectsAudioSource.playOnAwake = false;
            // We want full 3D sound.
            _effectsAudioSource.spatialBlend = 1.0f;
        }

        protected IEnumerator FlashObject(Color flashColor, float totalTimeToKeepFlashingBetweenColors, float delayBetweenColorChanges)
        {
            var flashingFor = 0.0f;
            var setFToFlashColor = true;
            while (flashingFor < totalTimeToKeepFlashingBetweenColors)
            {
                for (int i = 0; i < rendererList.Count; i++)
                {
                    //rendererList[i].material.color = setFToFlashColor ? flashColor : originalColors[i];
                    rendererList[i].material.SetColor(EmissionColor, setFToFlashColor ? flashColor : _originalColors[i]);
                }

                flashingFor += Time.deltaTime;
                yield return new WaitForSeconds(delayBetweenColorChanges);
                flashingFor += delayBetweenColorChanges;
                setFToFlashColor = !setFToFlashColor;


            }

            // Set back to original color after flashing is done
            for (int i = 0; i < rendererList.Count; i++)
            {
                //rendererList[i].material.color = originalColors[i];
                rendererList[i].material.SetColor(EmissionColor, _originalColors[i]);

            }
        }

    }
}
