using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[DisallowMultipleComponent]
public class SpawnPointManager : MonoBehaviour
{
    [SerializeField] private List<SpawnPoint> _spawnPoints;

    public void RegisterSpawnPoint(SpawnPoint spawnPoint)
    {
        _spawnPoints.Add(spawnPoint);
    }
    public SpawnPoint GetClosest(Vector3 position)

    {
        float bestDistance = float.MaxValue;
        SpawnPoint closestSpawnPoint = null;
        string closestName = "";
        Vector3 bestPosition = new Vector3(0,0,0);
        foreach (SpawnPoint spawnPoint in _spawnPoints)
        {
            if (!spawnPoint.active)
            {
                Debug.Log($"Spawnpoint {spawnPoint.name} is not activated, ignoring." );
                continue;
            }

            if (closestSpawnPoint != null)
            {
                closestName = closestSpawnPoint.name;
                
            }
            float currentDistance = Vector3.Distance(spawnPoint.transform.position, position);
            Debug.Log($"Comparing distance to spawnpoint {spawnPoint.name} at position {spawnPoint.transform.position} distance {currentDistance} with previous {closestName} {(closestSpawnPoint ? closestSpawnPoint.transform.position : null)} from player at {position}" + bestDistance);


            if (currentDistance < bestDistance)
            {
                
                Debug.Log($"Setting new best distance to spawnpoint with {spawnPoint.name}, distance {currentDistance} and less than previous distance " + bestDistance);

                bestDistance = currentDistance;
                closestSpawnPoint = spawnPoint;
            }
        }
        
        return closestSpawnPoint;
    }
}
