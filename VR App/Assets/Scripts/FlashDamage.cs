#pragma warning disable CS0649
using System.Collections;
using UnityEngine;
// ReSharper disable FieldCanBeMadeReadOnly.Local


namespace Assets.Scripts
{

    public class FlashDamage : MonoBehaviour
    {
        [SerializeField] private Color _startColor = new Color(207, 32, 32);

        [SerializeField] float _defaultFadeTime;
        float _fadeTime;
        
        private Renderer _renderer;
        private Color _color;
        private float _passedTime = 0;
        private bool _damageFlashInProgress;
        // Start is called before the first frame update
        void Start()
        {
            _renderer = GetComponent<MeshRenderer>();
        }
        public void Flash(float fadeTime =0) 
        {

            if (_damageFlashInProgress && fadeTime < _fadeTime)
                return;

            _damageFlashInProgress = true;
            _fadeTime = fadeTime == 0 ? _defaultFadeTime : fadeTime;
            Debug.Log("called with fade time " + fadeTime);
            if (_passedTime>0)
            {
                return;
            }
            
            _color = _startColor;
            _passedTime = 0;
            Coroutine aa = StartCoroutine(FadeFlash());
            
        }

        private IEnumerator FadeFlash()
        {
            while(_passedTime<_fadeTime)
            {
                
                _passedTime += Time.deltaTime;
                _color.a = Mathf.Lerp(_startColor.a, 0, _passedTime / _fadeTime);
                _renderer.material.color = _color;
                yield return null;
            }
            _passedTime = 0;
            _color.a = 0;
            _damageFlashInProgress = false;
        }

    }
}
