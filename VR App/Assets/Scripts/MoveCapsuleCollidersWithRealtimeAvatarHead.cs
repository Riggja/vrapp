using System.Collections.Generic;
using Normal.Realtime;
using UnityEngine;
using UnityEngine.Assertions;

namespace Assets
{
    public class MoveCapsuleCollidersWithRealtimeAvatarHead : MonoBehaviour
    { 
        public RealtimeAvatar NetworkAvatar;

        [SerializeField] private List<CapsuleCollider> collidersToMove;
        
        private float additionalHeight = 0.2f;
        // Start is called before the first frame update
        void Start()
        {
            Assert.IsNotNull(NetworkAvatar, "NetworkAvatar does not seem to have been assigned in the inspector on "  + gameObject.name);
            Assert.IsNotNull(NetworkAvatar.head, "No head found on NetworkAvatar assigned to ." + gameObject.name);
        }


        void CapsuleFollowHead()
        {
            Vector3 cameraInRigSpace = NetworkAvatar.transform.InverseTransformPoint(NetworkAvatar.head.transform.position);
            
            foreach (var coll in collidersToMove)
            {
                coll.height = cameraInRigSpace.y + additionalHeight;
             //   Debug.Log("Height set to coll" + coll.height + $" center {coll.center.x}, {-cameraInRigSpace.y / 2}, {coll.center.z}");
                coll.center = new Vector3(coll.center.x, cameraInRigSpace.y / 2, coll.center.z);
            }
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            CapsuleFollowHead();
        }
    }
}
